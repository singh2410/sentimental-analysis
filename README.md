# Sentimental Analysis
## By- Aarush Kumar
Sentimental Analysis of Twitter using live tweets.
Twitter allows businesses to engage personally with consumers. However, there’s so much data on Twitter that it can be hard for brands to prioritize mentions that could harm their business.
That's why sentiment analysis, a tool that automatically monitors emotions in conversations on social media platforms, has become a key instrument in social media marketing strategies. In this project I used live twitter data from developer account of twitter in order to perform analysis part.
### Thankyou!
