#!/usr/bin/env python
# coding: utf-8

# # Twitter Sentimental Analysis
# #By- Aarush Kumar
# #Dated: June 06,2021
# - Twitter data: First step is to gather twitter data.
# - Sentiment: To find out sentiments.
# - Analysis: Analysing of the data.

# In[1]:


get_ipython().system('pip install tweepy')
get_ipython().system('pip install textblob')


# In[2]:


get_ipython().system('pip install wordcloud')


# In[3]:


import tweepy
from textblob import TextBlob
import pandas as pd
import matplotlib.pyplot as plt
import re
from wordcloud import WordCloud


# In[4]:


# Twitter Api  Credentials
APIkey= "0kNz9t02xNm2QKw8V8jp26oCb"
APISecretKey= "PuHlKeUXR33TVxWloIwLHvXtNTmLbjhSnuMNEYciDDDULEZawZ"
accessToken= "1401506825497833472-guFRwQGyEnPdEQ1mSxjbhOObrygY31"
accessTokenSecret= "mzt84gG8MQn6Xh9ppODYrSVK04R7izYv7fetmyPUU4wI3"


# In[5]:


# create the authentication object
authenticate = tweepy.OAuthHandler(APIkey,APISecretKey)
authenticate.set_access_token(accessToken,accessTokenSecret)
api= tweepy.API(authenticate)


# In[6]:


tweets= api.user_timeline(screen_name='Trump',count=100,lang="en",tweet_mode='extended')


# In[7]:


i=1
for tweet in tweets[:10]:  
    print(str(i) + ') ' + tweet.full_text + '\n')
    i= i+1


# In[8]:


# Create dataframe with a column tweets
df= pd.DataFrame([tweet.full_text for tweet in tweets],columns=['Tweets'])
df


# In[9]:


# make a function to clean tweets
def cleanTxt(text):
    text= re.sub('@[A-Za-z0-9]+','',text ) #removing mentions
    text= re.sub("#",'',text) #removing #
    text= re.sub('RT[\s]+','',text) # removing Retweets
    text= re.sub('https?:\/\/\S+','',text) #removing links
    return text


# In[10]:


df['Tweets']= df['Tweets'].apply(cleanTxt)


# In[11]:


df


# In[12]:


analysis=TextBlob("Today was Sunday")
analysis.sentiment


# In[13]:


analysis=TextBlob("Today day was good ")
analysis.sentiment


# In[14]:


analysis=TextBlob("Today day was bad ")
analysis.sentiment


# In[15]:


analysis=TextBlob("Today day was best ")
analysis.sentiment


# In[16]:


analysis=TextBlob("Today day was worst ")
analysis.sentiment


# In[17]:


def getSubjectivity(text):
    return TextBlob(text).sentiment.subjectivity
def getPolarity(text):
    return TextBlob(text).sentiment.polarity
df['Subjectivity']= df['Tweets'].apply(getSubjectivity)
df['Polarity']= df['Tweets'].apply(getPolarity)


# In[18]:


df


# In[19]:


# Word Cloud Visualization
allwords= ' '.join([i for i in df['Tweets']])
Cloud= WordCloud(width=500,height=500,random_state=0,max_font_size=100).generate(allwords)
plt.imshow(Cloud)
plt.show()


# In[20]:


def getAnalysis(score):
    if score<0:
        return 'Negative'
    elif score==0:
        return 'Neutral'
    else:
        return 'Positive'
df['Analysis']= df['Polarity'].apply(getAnalysis)
df


# In[21]:


df[df['Analysis']=='Neutral']


# In[22]:


df['Analysis'].value_counts()


# In[23]:


df.shape


# In[24]:


df.size


# In[25]:


# plotting scatter plot
plt.figure(figsize=(8,6))
for i in range(0,df.shape[0]):
    plt.scatter(df['Polarity'][i],df['Subjectivity'][i],color='Blue')
plt.title("Sentiment Analysis")
plt.xlim(-1,1)
plt.xlabel('Polarity')
plt.ylabel('Subjectivity')
plt.show()


# In[26]:


df['Analysis'].value_counts().plot(kind='bar')
plt.title("Sentiment Analysis")
plt.xlabel('Polarity')
plt.ylabel('Count')
plt.show()


# In[27]:


# Lets get positive tweets only
i=1
sortedDF = df.sort_values(by=['Polarity'],ascending=False)
for j in range(0,sortedDF.shape[0]):
    if (sortedDF['Analysis'][j]=='Positive'):
        print(str(i)+ ') ' + sortedDF['Tweets'][j])
        print()
        i=i+1


# In[28]:


# Lets get negative tweets only
i=1
sortedDF = df.sort_values(by=['Polarity'],ascending=False)
for j in range(0,sortedDF.shape[0]):
    if (sortedDF['Analysis'][j]=='Negative'):
        print(str(i)+ ') ' + sortedDF['Tweets'][j])
        print()
        i=i+1


# In[29]:


# Lets get neutral tweets only
i=1
sortedDF = df.sort_values(by=['Polarity'],ascending=False)
for j in range(0,sortedDF.shape[0]):
    if (sortedDF['Analysis'][j]=='Neutral'):
        print(str(i)+ ') ' + sortedDF['Tweets'][j])
        print()
        i=i+1

